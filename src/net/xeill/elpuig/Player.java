package net.xeill.elpuig;

import java.util.ArrayList;
import java.util.Random;

/**
* This class implements the Player.
*
* @author  ASIX1
*/

public class Player {

    String name;
    int stack;
    ArrayList<Card> hand = new ArrayList<>();

    /**
    * Creates the player with name and stack
    */
    Player(String name, int stack) {
      this.name = name;
      this.stack = stack;
    }

    /**
    * Bets the number of chips indicated by the parameter quantity, and returns
    * the bet amounr. If this bet cannot be make it bet 0 and returns 0
    */
    public int bet(int quantity){
      // Si se puede hacer esa apuesta decrementamos la cantidad del stack
      if (quantity >= this.stack) {
        this.stack = this.stack - quantity;
        // Retornamos la cantidad finalmente apostada
        return quantity;
      } else {
      // Si no es posible hacer esa apuesta se retorna 0
        return 0;
      }
    }

    /**
    * Gets the Card card and add it to the player hand.
    */
    public void take(Card card){
      this.hand.add(card);
    }





}

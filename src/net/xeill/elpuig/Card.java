package net.xeill.elpuig;

/**
* This class implements the Card.
*
* @author  ASIX1
*/

public class Card {

    String name;     //13
    int value;
    String suit;     //4
    String symbol;

    Card(String name, int value, String suit, String symbol) {
      this.name = name;
      this.value = value;
      this.suit = suit;
      this.symbol = symbol;
    }
}

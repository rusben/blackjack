package net.xeill.elpuig;

import java.io.*;
import java.util.*;
import java.util.concurrent.TimeUnit ;

/**
* This class implements the Blackjack UI, User Interface.
*
* @author  ASIX1
*
*/

public class UserInterface {

  Scanner scanner = new Scanner(System.in);

  /**
  * Ask a name
  */
  String askName() {
    String name = scanner.nextLine();
    return name;
  }

  /**
  * Clears the screen.
  */
  void clearScreen() {
    System.out.print("\033\143");
  }

  /**
  * Shows the "YOU LOOSE" message
  */
  void showYouLoose() {
    System.out.println("\n\033[30;101m" +
                "  ╦ ╦╔═╗╦ ╦  ╦  ╔═╗╔═╗╔═╗  \n" +
                "  ╚╦╝║ ║║ ║  ║  ║ ║╚═╗║╣   \n" +
                "   ╩ ╚═╝╚═╝  ╩═╝╚═╝╚═╝╚═╝  \033[0m");
  }

  /**
  * Shows the "YOU WIN" message
  */
  void showYouWin(){
    System.out.println("\n\033[30;102m" +
              "  ╦ ╦╔═╗╦ ╦  ╦ ╦ ╦ ╔╗╔  \n" +
              "  ╚╦╝║ ║║ ║  ║║║ ║ ║║║  \n" +
              "   ╩ ╚═╝╚═╝  ╚╩╝ ╩ ╝╚╝  \033[0m");
  }

  /**
  * Shows the game status
  */
  void showStatus() {
  }

  void pressAnyKey(){
    scanner.nextLine();
  }

  /**
  * Pauses the game interaction during seconds
  */
  public void sleep(int seconds) {

    try {
      TimeUnit.SECONDS.sleep(seconds);
    } catch (Exception e) {

    }
  }

  public void print(Blackjack b) {
    System.out.println("||------------------------------------------------------------||");
    System.out.println("||-----------------------BLACKJACK----------------------------||");
    System.out.println("||____________________________________________________________||");
    System.out.println("||-----------PLAYERS-----------||-----------CROUPIER----------||");
    System.out.println("||-----------------------------||-----------"+b.croupier.name+"----------||");

    for (int i = 0; i < b.players.length; i++) {
      System.out.println("||----------"+b.players[i].name+"-----------||-----------------------------||");
    }

  }

}

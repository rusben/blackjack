package net.xeill.elpuig;

import java.io.*;
import java.util.*;

/**
* This class implements the Deck.
*
* @author  ASIX1
*/

public class Deck {
    ArrayList<Card> cards;

    Deck() {
      this.cards = new ArrayList<Card>();

      for (int i = 1; i <= 13; i++) {
        for (int j = 1; j <= 4; j++) {

          String name;
          String suit;
          String symbol;

          if (i == 1) {
             name = "A";
          } else if (i == 11) {
            name = "J";
          } else if (i == 12) {
            name = "Q";
          } else if (i == 13) {
            name = "K";
          } else {
            name = ""+i;
          }

          // 1 - diamonds (♦), 2 - clubs (♣), 3 - hearts (♥), 4 - spades (♠)
          if (j == 1) {
            suit = "diamonds";
            symbol = "♦";
          } else if (j == 2) {
            suit = "clubs";
            symbol = "♣";

          } else if (j == 2) {
            suit = "hearts";
            symbol = "♥";
          } else {
            suit = "spades";
            symbol = "♠";
          }

          cards.add(new Card(name, i, suit, symbol));
        }
      }

    }

    /**
    * Shuffles the cards in the Deck
    *
    * @return picks a card in the position i and swaps it with another in a
    *         random position.
    */
    public void shuffle(){
      for(int i = 0; i < this.cards.size();i++) {
        int j = (int)(Math.random() * this.cards.size()); // Get a random index out of 52
        Card temp = this.cards.get(i); // Get the element in the position i
        this.cards.set(i, this.cards.get(j)); // Set the element in with the value og the element in j
        this.cards.set(j, temp);
      }
    }

    /**
    * Gets the next Card in the Deck and removes it
    *
    * @return the next Card in the Deck, if there is not a Card anymore returns
    *         a {@code null} value
    */
    public Card next(){
      // Eliminamos el primer elemento del mazo y lo retornamos, solo si hay
      // cartas en el mazo.
      if (this.cards.size() > 0) {
        return this.cards.remove(0);
      } else {
        // Retornando null indicamos que no hay más cartas en el mazo.
        return null;
      }

    }

}

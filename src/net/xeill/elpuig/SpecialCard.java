package net.xeill.elpuig;

/**
* This class implements the SpecialCard.
*
* A special card is a card with multiple values depending on the user's will
*
* @author  ASIX1
*/

public class SpecialCard extends Card {
    int otherValue;

    SpecialCard(String name, int value, String suit, String symbol, int otherValue) {
      // Además los valores correspondientes a la carta se envían a la
      // superclase Card para que los inicialice en el objeto
      super(name, value, suit, symbol);
      // Este constructor asigna el valor otherValue a la carta especial
      this.otherValue = otherValue;
    }
}

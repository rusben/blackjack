package net.xeill.elpuig;

import java.util.*;
import java.io.*;

/**
* This class implements the Blackjack game.
*
* @author  ASIX1
*/

public class Blackjack {

  Deck deck;
  Croupier croupier;
  Player[] players;

  Blackjack(Croupier croupier, int nPlayers) {
    this.deck = new Deck();
    this.croupier = croupier;
    // Limitación de jugadores a 4.
    if (nPlayers > 4) nPlayers = 4;

    this.players = new Player[nPlayers];
  }

  public void initializePlayers() {

  }

  public void start() {

  }

  public void status() {

  }

  public void next() {

  }



}

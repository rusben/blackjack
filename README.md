# Blackjack
Blackjack Java Terminal is the next generation of terminal games.

## Key Features

* Win other players with no effort.
  - Instantly see how the table is changing.
* Enemies AI improved.
* Javadoc documentation available.
* A huge community behind the back.

## Complete documentarion there
https://rusben.gitlab.io/blackjack

## How to generate javadoc?

From the project root folder write the following command to generate the javadoc

```bash
# Go into the project root folder
$ cd blackjack/

# Generate javadoc
$ javadoc -d doc/elpuig/ -sourcepath src/ -subpackages net
```

## How to compile?
```bash
# Go into the project source folder (src)
$ cd blackjack/src

# Compile the Main class
$ javac net/xeill/elpuig/Blackjack.java

# Run the Main
$ java net.xeill.elpuig.Blackjack
```

### [FAQ]

#### Generate javadoc from command line

https://stackoverflow.com/questions/4592396/how-to-generate-javadoc-from-command-line

#### Javadoc example

http://www.docjar.net/html/api/java/util/Collections.java.html
